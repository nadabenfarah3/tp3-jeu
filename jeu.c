#include "jeu.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

char hasard() {
    srand(time(NULL));
    int random_choice = rand() % 3;

    switch (random_choice) {
        case 0:
            return 'R';
        case 1:
            return 'P';
        case 2:
            return 'C';
        default:
            return 'R'; // Par défaut, pour éviter les erreurs
    }
}
