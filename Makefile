CC=gcc
CFLAGS=-std=c99 -Wall
TESTFLAGS=-fprofile-arcs -ftest-coverage
testhasard: testhasard.c jeu.h jeu.c
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testhasard testhasard.c jeu.h
	./testhasard
	gcov -c -p testhasard
clean:
	rm -f *.o test *.gcov *.gcda *.gcno
